package it.com.atlassian.plugins.rest.sample.expansion;

import com.atlassian.plugins.rest.sample.expansion.UriBuilder;
import com.atlassian.plugins.rest.sample.expansion.entity.Game;
import com.atlassian.plugins.rest.sample.expansion.entity.Player;
import com.sun.jersey.api.client.Client;
import junit.framework.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 */
public class ExpansionTest
{
    private UriBuilder builder;

    @Before
    public void setUp()
    {
        builder = UriBuilder.create().path("rest").path("ex").path("1").param("os_username", "admin").param("os_password", "admin");
    }

    @Test
    public void testGetGameWithNonExpandedPlayers()
    {
        final Game game = Client.create().resource(builder.build()).path("game").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertNull(game.getPlayers().getPlayers());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertNull(game.getPlayers().getStartIndex());
    }

    @Test
    public void testGetGameWithExpandedPlayers()
    {
        final Game game = Client.create().resource(builder.build()).path("game").queryParam("expand", "players").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertEquals(5, game.getPlayers().getPlayers().size());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertEquals(0, game.getPlayers().getStartIndex().intValue());

        assertEquals(0, game.getPlayers().getPlayers().get(0).getId());
        assertEquals(4, game.getPlayers().getPlayers().get(4).getId());
    }

    @Test
    public void testGetGameWithExpandedPlayersAndOffset()
    {
        final Game game = Client.create().resource(builder.build()).path("game").queryParam("expand", "players[3:]").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertEquals(5, game.getPlayers().getPlayers().size());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertEquals(3, game.getPlayers().getStartIndex().intValue());

        assertEquals(3, game.getPlayers().getPlayers().get(0).getId());
        assertEquals(7, game.getPlayers().getPlayers().get(4).getId());
    }

    @Test
    public void testGetGameWithExpandedPlayersAndOffsetOutOfBounds()
    {
        final Game game = Client.create().resource(builder.build()).path("game").queryParam("expand", "players[12:]").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertNull(game.getPlayers().getPlayers());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertNull(game.getPlayers().getStartIndex());
    }

    @Test
    public void testGetPlayerNames()
    {
        final Player player1 = Client.create().resource(builder.build()).path("player").path("0").get(Player.class);
        final Player player2 = Client.create().resource(builder.build()).path("player").path("1").get(Player.class);
        assertEquals("Adam Ashley-Cooper", player1.getFullName());
        assertEquals("Matt Giteau", player2.getFullName());
    }

    @Test
    public void testPlayerWithRecordCanBeExpanded()
    {
        // get without expansion
        Player player = Client.create().resource(builder.build()).path("player").path("2").get(Player.class);
        Assert.assertNotNull(player.getRecord());
        // now expand it
        player = Client.create().resource(builder.param("expand", "record").build()).path("player").path("2").get(Player.class);
        Assert.assertNotNull(player.getRecord());
        Assert.assertEquals((Integer) 513, player.getRecord().getPointsScored());
    }

    @Test
    public void testPlayerWithNoRecordCanNotBeExpanded()
    {
        // get without expansion
        Player player = Client.create().resource(builder.build()).path("player").path("1").get(Player.class);
        Assert.assertNull(player.getRecord());
        // now expand it
        player = Client.create().resource(builder.param("expand", "record").build()).path("player").path("1").get(Player.class);
        Assert.assertNull(player.getRecord());
    }

}
