package com.atlassian.plugins.rest.sample.expansion.resource;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/**
 * A resource used to show details of a player and optionally an expandable player record element.  The
 * {@link DataStore} will provide data of two player, one which has a player record and which doesn't.
 *
 * When viewing player with the record the client has the option to expand it.
 */
@Path("/player")
@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
public class PlayerResource
{
    @Context
    private UriInfo uriInfo;

    @GET
    @Path("/{id}")
    public Response getPlayer(@PathParam("id") Integer id) throws Exception
    {
        return Response.ok(DataStore.getInstance().getPlayer(id, getPlayerUriBuilder(uriInfo))).build();
    }

    static UriBuilder getPlayerUriBuilder(UriInfo uriInfo)
    {
        return uriInfo.getBaseUriBuilder().path("player").path("{id}");
    }
}

