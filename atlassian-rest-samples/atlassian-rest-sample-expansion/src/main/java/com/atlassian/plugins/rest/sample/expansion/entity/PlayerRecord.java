package com.atlassian.plugins.rest.sample.expansion.entity;

import com.atlassian.plugins.rest.common.expand.Expander;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The total points scoring record for a player.
 */
@XmlRootElement
@XmlAccessorType(FIELD)
@Expander(PlayerRecordExpander.class)
public class PlayerRecord
{
    /**
     * Creates an empty record which is used to inform the client that a record is available without
     * performing the cost of calculating it.  If the client requires the record it can expand this field.
     *
     * @param player
     * @return
     */
    public static PlayerRecord emptyRecord(Player player)
    {
        return new PlayerRecord(player);
    }

    @XmlElement
    private Integer pointsScored;

    @XmlTransient
    private Player player;

    public PlayerRecord()
    {
    }

    public PlayerRecord(Player player)
    {
        this.player = player;
    }

    public PlayerRecord(int pointsScored)
    {
        this.pointsScored = pointsScored;
    }

    public Integer getPointsScored()
    {
        return pointsScored;
    }

    public void setPointsScored(Integer pointsScored)
    {
        this.pointsScored = pointsScored;
    }

    public Player getPlayer()
    {
        return player;
    }
}