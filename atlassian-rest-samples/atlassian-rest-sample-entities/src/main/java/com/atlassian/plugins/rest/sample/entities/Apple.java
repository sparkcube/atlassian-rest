package com.atlassian.plugins.rest.sample.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Apple
{
    String name;
    String reqContentType;
    String reqAccept;

    public Apple()
    {
    }

    public Apple(final String name, final String reqContentType, final String reqAccept)
    {
        this.name = name;
        this.reqContentType = reqContentType;
        this.reqAccept = reqAccept;
    }

    public String getName()
    {
        return name;
    }

    public String getReqContentType()
    {
        return reqContentType;
    }

    public String getReqAccept()
    {
        return reqAccept;
    }
}
