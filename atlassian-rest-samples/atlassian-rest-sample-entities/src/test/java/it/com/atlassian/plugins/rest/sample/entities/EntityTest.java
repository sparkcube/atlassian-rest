package it.com.atlassian.plugins.rest.sample.entities;

import com.atlassian.plugins.rest.sample.entities.EntityClientServlet;
import com.atlassian.plugins.rest.sample.entities.EntityResource;
import com.atlassian.plugins.rest.sample.entities.UriBuilder;
import com.sun.jersey.api.client.Client;
import org.junit.Test;

import java.net.URI;

import static com.atlassian.plugins.rest.sample.entities.EntityClientServlet.*;
import static org.junit.Assert.assertEquals;

/**
 * Testing {@link EntityResource}
 */
public class EntityTest
{
    /**
     * Simple test that executes {@link EntityClientServlet}, which uses the {@link com.atlassian.sal.api.net.RequestFactory} to query
     * {@link EntityResource}
     */
    @Test
    public void testApplesForOranges()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri).queryParam(P_ORANGE, "valencia").get(String.class);
        assertEquals(
                "apple-valencia\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/xml", returned);   // application/xml is the default content-type & accept header value
    }

    @Test
    public void testApplesForOrangesJson()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/json")
                .queryParam(P_ACCEPT, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/json\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesDefaultContentTypeAcceptJson()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_ACCEPT, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesExplicitXmlContentTypeAndAccept()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/xml")
                .queryParam(P_ACCEPT, "application/xml")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/xml", returned);
    }

    @Test
    public void testApplesForOrangesMissingAcceptHeaderDefaultsToContentType()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/json\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesMultipleValidAcceptHeaders()
    {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_ACCEPT, "application/json")
                .queryParam(P_ACCEPT, "application/xml")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" + //default
                        "Accept=application/json,application/xml", returned);
    }


}
