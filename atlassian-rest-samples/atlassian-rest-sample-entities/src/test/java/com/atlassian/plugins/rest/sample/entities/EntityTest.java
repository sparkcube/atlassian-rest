package com.atlassian.plugins.rest.sample.entities;

import org.junit.Test;

import javax.ws.rs.core.HttpHeaders;
import java.util.ArrayList;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class EntityTest
{
    private EntityResource entity = new EntityResource();

    @Test
    public void testApplesForOranges()
    {
        HttpHeaders headers = createMock(HttpHeaders.class);
        expect(headers.getRequestHeader("Content-Type")).andReturn(new ArrayList<String>()).once();
        expect(headers.getRequestHeader("Accept")).andReturn(new ArrayList<String>()).once();
        replay(headers);
        Apple apple = (Apple) entity.applesForOranges(new Orange("valencia"), headers).getEntity();
        assertEquals("apple-valencia", apple.getName());
        assertEquals("N/A", apple.getReqAccept());
        assertEquals("N/A", apple.getReqContentType());
        verify(headers);

        reset(headers);
        expect(headers.getRequestHeader("Content-Type")).andReturn(new ArrayList<String>()
        {{
                add("application/json");
            }}).once();
        expect(headers.getRequestHeader("Accept")).andReturn(new ArrayList<String>()
        {{
                add("application/xml");
            }}).once();
        replay(headers);
        apple = (Apple) entity.applesForOranges(new Orange("delfino"), headers).getEntity();
        assertEquals("apple-delfino", apple.getName());
        assertEquals("application/json", apple.getReqContentType());
        assertEquals("application/xml", apple.getReqAccept());
        verify(headers);

        reset(headers);
        expect(headers.getRequestHeader("Content-Type")).andReturn(new ArrayList<String>()
        {{
                add("application/json");
            }}).once();
        expect(headers.getRequestHeader("Accept")).andReturn(new ArrayList<String>()
        {{
                add("application/xml");
                add("application/json");
                add("image/png");
            }}).once();
        replay(headers);
        apple = (Apple) entity.applesForOranges(new Orange("delfino"), headers).getEntity();
        assertEquals("apple-delfino", apple.getName());
        assertEquals("application/json", apple.getReqContentType());
        assertEquals("application/xml,application/json,image/png", apple.getReqAccept());
        verify(headers);
    }

}
