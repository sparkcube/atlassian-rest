package com.atlassian.plugins.rest.helloworld;

import com.atlassian.plugins.rest.common.security.CorsAllowed;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;

import java.security.Principal;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

@Path ("/helloworld")

public class HelloWorld
{
    @GET
    @Produces ("text/plain")
    @Path ("/authenticated")
    public String getAuthenticatedMessage()
    {
        return "Goodbye Authenticated World";
    }

    @GET
    @Produces ("text/plain")
    @Path ("/anonymous")
    @AnonymousAllowed
    @CorsAllowed
    public String getAnonymousMessage()
    {
        return "Goodbye Anonymous World";
    }

    @GET
    @Produces ("text/plain")
    @Path ("/admin")
    @AnonymousAllowed
    public String getMessageForAdmin(@Context AuthenticationContext authenticationContext)
    {
        checkIsUser(authenticationContext, "admin");
        return "Goodbye " + authenticationContext.getPrincipal();
    }

    private void checkIsUser(AuthenticationContext context, String userName)
    {
        final Principal principal = context.getPrincipal();
        if (principal == null || !principal.getName().equals(userName))
        {
            throw new SecurityException("You're not '" + userName + "' I know who you really are'" + principal + "', you can't access this information");
        }
    }
}