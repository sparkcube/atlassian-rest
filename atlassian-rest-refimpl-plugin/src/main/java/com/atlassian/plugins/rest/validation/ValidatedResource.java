package com.atlassian.plugins.rest.validation;

import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.transaction.TransactionInterceptor;
import com.atlassian.plugins.rest.common.validation.ValidationInterceptor;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/validatedResource")
@InterceptorChain({ValidationInterceptor.class, TransactionInterceptor.class})
public class ValidatedResource
{
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    public PersonResponse addPerson(Person person)
    {
        PersonResponse res = new PersonResponse();
        res.setName(person.getName());
        return res;
    }
}