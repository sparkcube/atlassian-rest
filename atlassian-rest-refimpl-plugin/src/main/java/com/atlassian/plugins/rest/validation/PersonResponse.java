package com.atlassian.plugins.rest.validation;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PersonResponse
{
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}