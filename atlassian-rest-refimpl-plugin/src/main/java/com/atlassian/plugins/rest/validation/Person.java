package com.atlassian.plugins.rest.validation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Person
{
    @NotNull
    @Size(min = 2, max = 10, message = "person.error.name.wrongSize")
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
