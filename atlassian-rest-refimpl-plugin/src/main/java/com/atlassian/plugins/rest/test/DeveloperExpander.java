package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.AbstractRecursiveEntityExpander;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapperCallBacks;
import com.google.common.collect.Lists;

/**
 * A simple developer expander. It expands developers always the same way. It is just used for testing purpose.
 */
public class DeveloperExpander extends AbstractRecursiveEntityExpander<Developer>
{
    protected Developer expandInternal(Developer developer)
    {
        developer.setFullName("A developer full name");
        developer.setEmail("developer@example.com");
        developer.setFavouriteDrinks(new FavouriteDrinks(1, ListWrapperCallBacks.ofList(Lists.newArrayList(new FavouriteDrink("coffee")))));
        return developer;
    }
}
