package com.atlassian.plugins.rest.interceptor;

import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.atlassian.plugins.rest.common.interceptor.ResourceInterceptor;

import java.lang.reflect.InvocationTargetException;

public class MessageInterceptor implements ResourceInterceptor
{
    private final NameProviderService nameProviderService;

    public MessageInterceptor(NameProviderService nameProviderService)
    {
        this.nameProviderService = nameProviderService;
    }

    public void intercept(MethodInvocation invocation) throws IllegalAccessException, InvocationTargetException
    {
        invocation.invoke();
        final Message message = (Message) invocation.getHttpContext().getResponse().getEntity();
        message.setMessage(message.getMessage() + ": " + nameProviderService.getName());
    }
}
