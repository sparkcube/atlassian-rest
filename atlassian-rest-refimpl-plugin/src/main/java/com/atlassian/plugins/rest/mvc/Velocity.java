package com.atlassian.plugins.rest.mvc;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Path;

@Path("velocity")
@AnonymousAllowed
public class Velocity
{
    public static final String TITLE = "This is a Title";
    public static final String SUB_TITLE = "This is a sub-title";

    public String getTitle()
    {
        return TITLE;
    }

    public String getSubtitle()
    {
        return SUB_TITLE;
    }
}
