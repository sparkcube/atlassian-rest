package com.atlassian.plugins.rest.xsrf;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

/**
 */
@Path ("/xsrfCheck")
@AnonymousAllowed
public class XsrfCheck
{
    @GET
    @RequiresXsrfCheck
    public String getXsrfMessage()
    {
        return "Request succeeded";
    }

    @POST
    @RequiresXsrfCheck
    public String postXsrfMessage()
    {
        return "Request succeeded";
    }

    @PUT
    @RequiresXsrfCheck
    public String putXsrfMessage()
    {
        return "Request succeeded";
    }

    @DELETE
    @RequiresXsrfCheck
    public String deleteXsrfMessage()
    {
        return "Request succeeded";
    }
}
