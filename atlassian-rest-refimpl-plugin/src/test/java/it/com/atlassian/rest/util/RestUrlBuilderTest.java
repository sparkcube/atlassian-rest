package it.com.atlassian.rest.util;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.WebResource;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RestUrlBuilderTest
{
    private WebResource webResource;

    @Before
    public void setUp()
    {
        webResource = WebResourceFactory.authenticated();
    }

    @Test
    public void testDummyResource()
    {
        assertEquals("http://atlassian.com:1234/foo/dummy/sub",
                webResource.path("restUrlBuilder/dummyResource").get(String.class));
    }
}
