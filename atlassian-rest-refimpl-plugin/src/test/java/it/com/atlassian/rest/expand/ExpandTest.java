package it.com.atlassian.rest.expand;

import com.atlassian.plugins.rest.expand.Expand;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.google.common.collect.Lists;
import com.sun.jersey.api.client.WebResource;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Test for expand functionality.
 */
public class ExpandTest
{
    private WebResource webResource;

    @Before
    public void setUp()
    {
        webResource = WebResourceFactory.authenticated();
    }

    @Test
    public void selfExpandingFieldGetsExpanded()
    {
        Expand expand = webResource.path("expand").queryParam("expand", "selfExpanding").get(Expand.class);

        assertTrue(Lists.<Object>newArrayList(Expand.SELF_EXPANDING_STRINGS).equals(expand.getSelfExpanding()));
    }
}
