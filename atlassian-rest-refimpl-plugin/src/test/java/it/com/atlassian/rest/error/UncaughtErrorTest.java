package it.com.atlassian.rest.error;

import com.atlassian.rest.jersey.client.WebResourceFactory;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UncaughtErrorTest
{
    private static final String MESSAGE = "my error detail string";
    private static final String JSON_ERROR = "{\"message\":\"" + MESSAGE + "\",\"status-code\":500}";
    private static final String XML_ERROR = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
        + "<status><status-code>500</status-code><message>" + MESSAGE + "</message></status>";
    
    @Test
    public void testUncaughtErrorResponseIsReturnedInHtmlByDefault()
    {
        final ClientResponse response = WebResourceFactory.anonymous()
            .path("errors/uncaughtInternalError")
            .queryParam("message", MESSAGE)
            .get(ClientResponse.class);
        assertEquals(500, response.getStatus());
        assertTrue(response.getType().toString().startsWith("text/html"));
        // The default HTML version of the error response does not include any error details;
        // it's just the Tomcat "internal error" page.  This is probably desirable behavior
        // since hitting a REST resource from a browser is not an intended use case.
        assertFalse(response.getEntity(String.class).contains(MESSAGE));
    }
    
    @Test
    public void testUncaughtErrorResponseIsReturnedInXMLWhenXMLIsRequested()
    {
        final ClientResponse response = WebResourceFactory.anonymous()
            .path("errors/uncaughtInternalError")
            .queryParam("message", MESSAGE)
            .accept("application/xml")
            .get(ClientResponse.class);
        assertEquals(500, response.getStatus());
        assertEquals("application/xml", response.getType().toString());
        assertEquals(XML_ERROR, response.getEntity(String.class));
    }
    
    @Test
    public void testUncaughtErrorResponseIsReturnedInJSONWhenJSONIsRequested()
    {
        final ClientResponse response = WebResourceFactory.anonymous()
            .path("errors/uncaughtInternalError")
            .queryParam("message", MESSAGE)
            .accept("application/json")
            .get(ClientResponse.class);
        assertEquals(500, response.getStatus());
        assertEquals("application/json", response.getType().toString());
        assertEquals(JSON_ERROR, response.getEntity(String.class));
    }
}
