package it.com.atlassian.rest.xsrf;

import com.atlassian.plugins.rest.json.JsonObject;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

import static org.junit.Assert.*;

/**
 */
public class XsrfCheckTest
{
    private WebResource.Builder webResource;

    @Before
    public void setUp()
    {
        webResource = WebResourceFactory.anonymous().path("xsrfCheck").getRequestBuilder();
    }

    @Test
    public void testGetBlocked()
    {
        assertBlocked("GET", webResource);
    }

    @Test
    public void testGetSuccess()
    {
        assertSuccessful("GET", addXsrf(webResource));
    }

    @Test
    public void testPostFormBlocked()
    {
        assertBlocked("POST", webResource.header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED));
    }

    @Test
    public void testPostFormSuccess()
    {
        assertSuccessful("POST", addXsrf(webResource).header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED));
    }

    @Test
    public void testPostJsonSuccess()
    {
        assertSuccessful("POST", webResource.entity(new JsonObject()));
    }

    @Test
    public void testPutFormSuccess()
    {
        assertSuccessful("PUT", webResource.header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED));
    }

    private WebResource.Builder addXsrf(WebResource.Builder webResource)
    {
        return webResource.header("X-Atlassian-Token", "nocheck");
    }

    private void assertSuccessful(String method, WebResource.Builder webResource)
    {
        assertEquals("Request succeeded", webResource.method(method, String.class));
    }

    private void assertBlocked(String method, WebResource.Builder webResource)
    {
        try
        {
            webResource.method(method, String.class);
            fail("Request succeeded");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals("XSRF check failed", e.getResponse().getEntity(String.class));
        }
    }
}
