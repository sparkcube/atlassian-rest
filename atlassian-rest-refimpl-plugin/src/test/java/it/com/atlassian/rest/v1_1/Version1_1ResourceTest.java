package it.com.atlassian.rest.v1_1;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;

public class Version1_1ResourceTest
{
    @Test
    public void testGet1_1Resource()
    {
        WebResource webResource10 = WebResourceFactory.authenticated(WebResourceFactory.REST_VERSION);
        WebResource webResource11 = WebResourceFactory.authenticated(WebResourceFactory.REST_VERSION + ".1");

        try
        {
            webResource10.path("version1_1Resource").get(String.class);
            fail("Should have failed due to missing resource");
        }
        catch (UniformInterfaceException ex)
        {
            assertEquals(404, ex.getResponse().getStatus());
        }

        assertEquals("Version 1.1 Hello World", webResource11.path("version1_1Resource").get(String.class));
    }
}