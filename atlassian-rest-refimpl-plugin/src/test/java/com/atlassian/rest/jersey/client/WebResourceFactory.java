package com.atlassian.rest.jersey.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;

/**
 * Gets {@link WebResource web resources} for testing.
 */
public final class WebResourceFactory
{
    public static final String LATEST = "latest";
    public static final String REST_VERSION = System.getProperty("refimpl.rest.version", "1");
    public static final String REST_VERSION_2 = System.getProperty("refimpl2.rest.version", "2");

    private static final String OS_USERNAME_QUERY_PARAM = "os_username";
    private static final String OS_PASSWORD_QUERY_PARAM = "os_password";

    private static final String ADMIN_USERNAME = "admin";
    private static final String ADMIN_PASSWORD = "admin";
    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");

    static
    {
        System.out.println("Base URI (latest) is <" + getWebResourceUri(LATEST) + ">");
        System.out.println("Base URI (version 1) is <" + getWebResourceUri(REST_VERSION) + ">");
        System.out.println("Base URI (version 2) is <" + getWebResourceUri(REST_VERSION_2) + ">");
    }

    public static WebResource authenticated(String version)
    {
        return authenticate(anonymous(version), ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    public static WebResource authenticated()
    {
        return authenticated(REST_VERSION);
    }

    public static WebResource anonymous(String version)
    {
        return Client.create().resource(getWebResourceUri(version));
    }

    public static WebResource anonymous()
    {
        return anonymous(REST_VERSION);
    }

    public static UriBuilder getUriBuilder()
    {
        return UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT);
    }

    private static URI getWebResourceUri(String version)
    {
        return getUriBuilder().path("rest").path("refimpl").path(version).build();
    }

    public static WebResource authenticate(WebResource webResource, String username, String password)
    {
        return webResource.queryParam(OS_USERNAME_QUERY_PARAM, username).queryParam(OS_PASSWORD_QUERY_PARAM, password);
    }
}
